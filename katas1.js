//1
function oneThroughTwenty() {
    
   /* Your code goes below
   Write a for or a while loop
   return the result*/
  let count = []
  for (let i = 0; i < 20; i++){
    count[i] = i + 1
  }
  return console.log(count)
}
//call function oneThroughTwenty
oneThroughTwenty()


//2
function evensToTwenty() {
    
   /* Your code goes below
   Write a for or a while loop
   return the result */

  let count = []
  for (let i = 0; i <= 20; i++){
    if((i+1)%2 === 0){
      if(i>0){
        count[i] = i+1
      }
    }
  }
  return console.log(count)
}
//call function evensToTwenty
evensToTwenty()


//3
function oddsToTwenty() {
    
  /* Your code goes below
  Write a for or a while loop
  return the result */

  let count = []
  for (let i = 0; i <= 20; i++){
    if((i+1)%2 === 0){
      if(i>0){
        count[i] = i
      }
    }
  }
  return console.log(count)
}
//call function oddsToTwenty
oddsToTwenty()


//4
function multiplesOfFive() {

  let count = []
  for(let i = 0; i < 100; i++){
    if((i+1)%5 === 0){
      count[i] = i+1
    }
  }
  return console.log(count)
}
//call function multiplesOfFive
multiplesOfFive()


//5
function squareNumbers() {

  let count = []
  for(let i = 1; i <= 10; i++){
    count[i-1] = i*i
  }
  return console.log(count)
}
//call function squareNumbers
squareNumbers()


//6
function countingBackwards() {

  let count = []
  let number = 20
  for(let i = 0; i < 20; i++){
    count[i] = number
    number--
  }
  return console.log(count)
}
//call function countingBackwards
countingBackwards()


//7
function evenNumbersBackwards() {

  let count = []
  let number = 20
  for(let i = 0; i < 20; i+=2){
    count[i] = number
    number-=2
  }
  return console.log(count)

}
//call function evenNumbersBackwards
evenNumbersBackwards()


//8
function oddNumbersBackwards() {
  
  let count = []
  let number = 20
  for (let i = 0; i <= 20; i++){
    if((i+1)%2 === 0){
      if(i>0){
        count[i] = number
      }
    }
    number--
  }
  return console.log(count)
}
//call function oddNumbersBackwards
oddNumbersBackwards()


//9
function multiplesOfFiveBackwards() {
    
  let count = []
  let number = 100
  for(let i = 0; i < 100; i++){
    if((i)%5 === 0){
      count[i] = number
    }
    number--
  }
  return console.log(count)
}
//call function multiplesOfFiveBackwards
multiplesOfFiveBackwards()


function squareNumbersBackwards() {

  let count = []
  let j = 10
  for(let i = 1; i <= 10; i++){
    count[i-1] = j*j
    j--
  }
  return console.log(count)
}
//call function squareNumbersBackwards
squareNumbersBackwards()